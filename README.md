# Data source layer for vehicle-charging-station #

This project contains job to create a table and feed this table from csv file.

### How can I start service? ###

* set proper value in ```application.properties``` to connect to MySql database
* create database ```charging_station``` (or if you prefer another name but then be sure that in application.properties is the right database name)
* mvn clean install
* mvn spring-boot:run

### config size of chunk ###
* you can set size of chunk in application.properties (property ```chunk.size```)
