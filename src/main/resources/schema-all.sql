CREATE TABLE IF NOT EXISTS electric_vehicle_charging_station_usage (
    id INT AUTO_INCREMENT PRIMARY KEY,
    station_name VARCHAR(100),
    mac_address VARCHAR(100),
    start_date DATETIME,
    start_time_zone VARCHAR(10),
    end_date DATETIME,
    end_time_zone VARCHAR(10),
    transaction_date DATETIME,
    total_duration TIME,
    charging_time TIME,
    energy DECIMAL(20,10),
    user_id VARCHAR(100),
    evse_id VARCHAR(100)
);