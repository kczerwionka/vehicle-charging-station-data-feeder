package com.intel.vehicle_charging_station.data_feeder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataFeederApplication {
    public static void main(String[] args) {
        System.exit(SpringApplication.exit(SpringApplication.run(DataFeederApplication.class, args)));
    }
}
