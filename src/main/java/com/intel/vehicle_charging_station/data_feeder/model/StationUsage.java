package com.intel.vehicle_charging_station.data_feeder.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StationUsage {
    private String stationName;
    private String MACAddress;
    private String orgName;
    private String startDate;
    private String startTimeZone;
    private String endDate;
    private String endTimeZone;
    /* Pacific Time */
    private String transactionDate;
    private String totalDuration;
    private String chargingTime;
    /* kWh */
    private String energy;
    /* kg */
    private String GHGSavings;
    /* gallons */
    private String gasolineSavings;
    private String portType;
    private String portNumber;
    private String plugType;
    private String evseId;
    private String address;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private String latitude;
    private String longitude;
    private String currency;
    private String fee;
    private String endedBy;
    private String plugInEventId;
    private String driverPostalCode;
    private String userId;
    private String county;
    private String systemSN;
    private String modelNumber;
}
