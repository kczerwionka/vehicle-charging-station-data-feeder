package com.intel.vehicle_charging_station.data_feeder.mapping;

import com.intel.vehicle_charging_station.data_feeder.model.StationUsage;
import org.springframework.batch.item.ItemProcessor;

import java.text.SimpleDateFormat;

public class StationUsageItemProcessor implements ItemProcessor<StationUsage, StationUsage> {
    @Override
    public StationUsage process(StationUsage stationUsage) throws Exception {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String endDate = outputDateFormat.format(inputDateFormat.parse(stationUsage.getEndDate()));
        String startDate = outputDateFormat.format(inputDateFormat.parse(stationUsage.getStartDate()));
        String transactionDate = outputDateFormat.format(inputDateFormat.parse(stationUsage.getTransactionDate()));
        return StationUsage.builder()
                .stationName(stationUsage.getStationName())
                .MACAddress(stationUsage.getMACAddress())
                .startDate(startDate)
                .startTimeZone(stationUsage.getStartTimeZone())
                .endDate(endDate)
                .endTimeZone(stationUsage.getEndTimeZone())
                .transactionDate(transactionDate)
                .totalDuration(stationUsage.getTotalDuration())
                .chargingTime(stationUsage.getChargingTime())
                .energy(stationUsage.getEnergy())
                .userId(stationUsage.getUserId())
                .evseId(stationUsage.getEvseId())
                .build();
    }
}
