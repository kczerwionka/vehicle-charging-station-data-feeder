package com.intel.vehicle_charging_station.data_feeder.configuration;

import com.intel.vehicle_charging_station.data_feeder.listener.JobCompletionNotificationListener;
import com.intel.vehicle_charging_station.data_feeder.mapping.StationUsageItemProcessor;
import com.intel.vehicle_charging_station.data_feeder.model.StationUsage;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Value("${chunk.size}")
    private Integer chunkSize;

    @Bean
    public FlatFileItemReader<StationUsage> reader() {
        return new FlatFileItemReaderBuilder<StationUsage>()
                .name("stationUsageItemReader")
                .resource(new ClassPathResource("ElectricVehicleChargingStationUsageJuly2011Dec2020_2797601616445997881.csv"))
                .delimited()
                .names("stationName", "MACAddress", "orgName", "startDate", "startTimeZone", "endDate", "endTimeZone",
                        "transactionDate", "totalDuration", "chargingTime", "energy", "GHGSavings", "gasolineSavings",
                        "portType", "portNumber", "plugType", "evseId", "address", "city", "state", "postalCode",
                        "country", "latitude", "longitude", "currency", "fee", "endedBy", "plugInEventId",
                        "driverPostalCode", "userId", "county", "systemSN", "modelNumber"
                )
                .fieldSetMapper(new BeanWrapperFieldSetMapper<StationUsage>() {{
                    setTargetType(StationUsage.class);
                }})
                .linesToSkip(1)
                .build();
    }

    @Bean
    public StationUsageItemProcessor processor() {
        return new StationUsageItemProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<StationUsage> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<StationUsage>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO electric_vehicle_charging_station_usage " +
                        "(station_name, mac_address, start_date, start_time_zone, end_date, end_time_zone" +
                        ",transaction_date, total_duration, charging_time, energy, user_id, evse_id) VALUES (:stationName, :MACAddress, :startDate" +
                        ", :startTimeZone, :endDate, :endTimeZone, :transactionDate, :totalDuration, :chargingTime" +
                        ", :energy, :userId, :evseId)")
                .dataSource(dataSource)
                .build();
    }

    @Bean
    public Job importStationInformation(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importStationInformation")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<StationUsage> writer) {
        return stepBuilderFactory.get("step1")
                .<StationUsage, StationUsage>chunk(chunkSize)
                .reader(reader())
                .processor(processor())
                .writer(writer)
                .build();
    }
}
